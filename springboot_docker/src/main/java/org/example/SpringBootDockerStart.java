package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author gaogq
 */
@SpringBootApplication
public class SpringBootDockerStart {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerStart.class,args);
    }
}
