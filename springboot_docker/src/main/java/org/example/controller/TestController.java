package org.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @since 2021/12/21 18:56
 */
@RestController
@RequestMapping("/")
public class TestController {

    @RequestMapping("/test")
    public String test() {
        return "Hello Docker aaaaaa!";
    }
}
