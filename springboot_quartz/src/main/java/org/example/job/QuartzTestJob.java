package org.example.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试任务
 */
@Slf4j
public class QuartzTestJob extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String userName = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("userName");
        log.info("执行定时任务，userName:{}",userName);
    }
}
