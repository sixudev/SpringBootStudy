package org.example.service;

import org.example.entity.User;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

/**
 * 生产者提供服务
 */
@Service //属于Dubbo的@Service注解，非Spring  作用：暴露服务
@Component
public class UserServiceImpl implements UserService{

    @Override
    public User getUserById(String id) {
        if("1".equals(id)) {
            User user = new User();
            user.setId("1");
            user.setAddress("okokok");
            user.setName("小名");
            return user;
        }
        return null;
    }
}
