package cn.blogsx.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class QuickStart {

    @Value("${server.port}")
    private String port;

    @RequestMapping("/test")
    @ResponseBody
    public String hello(HttpServletRequest request) {
        return "当前服务端口"+port;
    }
}
