package cn.blogsx.schedule.component;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ScheduleConfig {

    @Scheduled(cron="0/2 * * * * ?")//Cron表达式 每2秒执行一次
    public void schedule(){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        String currentTime = df.format(time);
        System.out.println(currentTime+"===定时任务执行！！！");
    }
}
