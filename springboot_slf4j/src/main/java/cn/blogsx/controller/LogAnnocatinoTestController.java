/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cn.blogsx.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 日志注解测试类
 */
@Slf4j
@RestController
@RequestMapping("/anno")
public class LogAnnocatinoTestController {

    @RequestMapping("/test")
    private String test() {
        log.trace("trace级别日志注解生效---");
        log.debug("debug级别日志注解生效---");
        log.info("info级别日志注解生效---");
        log.warn("warn级别日志注解生效---");
        log.error("INfO级别日志注解生效---");
        return "日志注解执行---";
    }

}
