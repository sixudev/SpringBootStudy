package cn.blogsx.controller;

import cn.blogsx.dao.BookDao;
import cn.blogsx.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookDao bookDao;
    @RequestMapping("/test1")
    @ResponseBody
    public List<Book> test1(){
        List<Book> books = new ArrayList<>();

        Book book1 = new Book();
        book1.setId(1);
        book1.setName("蛙");
        book1.setAuthor("莫言");
        books.add(book1);

        Book book2 = new Book();
        book2.setId(2);
        book2.setName("丰乳肥臀");
        book2.setAuthor("莫言");
        books.add(book2);

        bookDao.insert(books);
        List<Book> books1 = bookDao.findByAuthorContains("莫言");
        System.out.println(books1);
        return books1;
//        Book books2 = bookDao.findByNameEquals("蛙");
//        System.out.println(books2);
    }
}
