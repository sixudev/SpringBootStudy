package cn.blogsx.kafka.admin;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.config.ConfigResource;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class AdminSample {

    public static final String TOPIC_NAME = "QQ_topic";

    public static void main(String[] args) throws Exception {
        // AdminClient adminClient = AdminSample.adminClient();
        //System.out.println(adminClient);
        //AdminSample.createTopic();
        //删除topic实例
        //delTopics();
//        topicList();
        //描述topic
//        describeTopics();

        //查看config
        describeConfig();
    }

    /**
     * 创建topic实例
     */
    public static void createTopic() {
        AdminClient adminClient = adminClient();
        //副本因子
        Short rs = 1;
        NewTopic newTopic = new NewTopic("BB_TOPICS", 1, rs);
        CreateTopicsResult topics = adminClient.createTopics(Arrays.asList(newTopic));
        System.out.println(topics);
    }

    /**
     * 获取topic列表
     */
    public static void topicList() throws Exception {
        AdminClient adminClient = adminClient();
        ListTopicsOptions options = new ListTopicsOptions();
        options.listInternal(true);
        ListTopicsResult listTopicsResult = adminClient.listTopics(options);
//        ListTopicsResult listTopicsResult = adminClient.listTopics();
        Set<String> strings = listTopicsResult.names().get();
        Collection<TopicListing> topicListings = listTopicsResult.listings().get();
        //打印names
        strings.stream().forEach(System.out::println);
        //打印topicListings
        topicListings.stream().forEach((topicList) -> {
            System.out.println(topicList);
        });
    }

    public static void describeConfig() throws Exception {
        AdminClient adminClient = adminClient();
        ConfigResource configResource = new ConfigResource(ConfigResource.Type.TOPIC, "AA_TOPICS");
        DescribeConfigsResult describeConfigsResult = adminClient.describeConfigs(Arrays.asList(configResource));
        Map<ConfigResource, Config> configResourceConfigMap = describeConfigsResult.all().get();
        Set<Map.Entry<ConfigResource, Config>> entries = configResourceConfigMap.entrySet();
        entries.stream().forEach((entry) -> {
            System.out.println("configResource:" + entry.getKey() + ",config:" + entry.getValue());
        });
    }

    /*
     描述Topic
  */
    public static void describeTopics() throws Exception {
        AdminClient adminClient = adminClient();
        DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(Arrays.asList("AA_TOPICS"));
        Map<String, TopicDescription> stringTopicDescriptionMap = describeTopicsResult.all().get();
        Set<Map.Entry<String, TopicDescription>> entries = stringTopicDescriptionMap.entrySet();
        entries.stream().forEach((entry) -> {
            System.out.println("name:" + entry.getKey() + ",desc:" + entry.getValue());
        });
    }

    /*
        删除topic
     */
    public static void delTopics() throws Exception {
        AdminClient adminClient = adminClient();
        DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Arrays.asList("alex-topic"));
        deleteTopicsResult.all().get();
        System.out.println();
    }

    public static AdminClient adminClient() {
        //设置Adminclient
        Properties properties = new Properties();
        properties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.0.115:9092");


        AdminClient adminClient = AdminClient.create(properties);
        return adminClient;
    }
}
