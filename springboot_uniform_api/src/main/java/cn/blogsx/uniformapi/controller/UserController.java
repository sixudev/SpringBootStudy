package cn.blogsx.uniformapi.controller;

import cn.blogsx.uniformapi.entity.User;
import cn.blogsx.uniformapi.exception.APIException;
import cn.blogsx.uniformapi.service.UserService;
import cn.blogsx.uniformapi.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/addUser")
    public String addUser(@RequestBody @Valid User user) {
        return userService.addUser(user);
    }
    @PostMapping("/testApiException")
    public String testApiException() throws Exception {
        try {
            int i=10/0;
        }catch (Exception e){
            throw new APIException(5000,"异常原因："+e.getMessage());
        }
        return null;
    }

    @GetMapping("/getUser")
    public User getUser() {
        User user = new User();
        user.setId(1L);
        user.setAccount("12345678");
        user.setPassword("12345678");
        user.setEmail("123@qq.com");
        return user;
    }
    @GetMapping("/getUser2")
    public ResultVO<User[]> getUser2() {
        User [] users = new User[2];
        User user1 = new User();
        user1.setId(1L);
        user1.setAccount("12345678");
        user1.setPassword("12345678");
        user1.setEmail("123@qq.com");
        users[0]=user1;

        User user2 = new User();
        user2.setId(1L);
        user2.setAccount("12345678");
        user2.setPassword("12345678");
        user2.setEmail("123@qq.com");
        users[1]=user2;
        return new ResultVO<>(users);
    }
}
