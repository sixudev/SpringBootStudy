package org.example.service;

import java.util.UUID;

import org.example.entity.Order;
import org.example.entity.User;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;

/**
 * 订单服务实现类
 */
@Service // spring框架提供的注解
public class OrderServiceImpl implements OrderService{

    @Reference // dubbo 注解
    private UserService userService;

    @Override
    public Order generateOrder(String userId) {
        Order order = new Order();
        User user = userService.getUserById(userId);
        if(user != null) {
            String orderId = UUID.randomUUID().toString();
            order.setOrderId(orderId);
            order.setAddress(user.getAddress());
            order.setUserName(user.getName());
            order.setUserId(user.getId());
        }
        return order;
    }
}
