package cn.blogsx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {
    @RequestMapping("/userLogin")
//    @ResponseBody
    public Map<String,Object> loginPage() {
        HashMap<String,Object> map = new HashMap<>();
        map.put("msg","用户未登录，请登陆！");
        return map;
    }

    @RequestMapping("/logout_res")
    @ResponseBody
    public Map<String,Object> logOut() {
        System.out.println("logout_res！");
        HashMap<String,Object> map = new HashMap<>();
        map.put("msg","用户已登出");
        return map;
    }
    @RequestMapping("/login")
    public String login() {
        return "login";
    }
}
