package cn.blogsx.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/admin/hello")
    public String adminHello() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        WebAuthenticationDetails details = (WebAuthenticationDetails) authentication.getDetails();
        System.out.println("IP地址为："+details.getRemoteAddress());
        return "hello admin1";
    }
    @RequestMapping("/user/hello")
    public String userHello() {
        return "hello user!";
    }
    @RequestMapping("/db/hello")
    public String dbHello() {
        return "hello dba!";
    }
    @RequestMapping("/hello")
    public String hello() {
        return "hello";
    }
}
