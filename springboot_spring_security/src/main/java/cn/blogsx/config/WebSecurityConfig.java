package cn.blogsx.config;

import cn.blogsx.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Configuration //Spring Security 拦截和授权管理配置类
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userService;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10); //配置加密密码
    }
    @Bean //该bean可结合.sessionManagement().maximumSessions(1)配置完成踢出登陆功能
    HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin/**")
                .hasRole("admin")
                .antMatchers("/user/**")
                .hasRole("user")
                .antMatchers("/db/**")
                .hasRole("dba")
                .anyRequest()
                .authenticated()
                .and()
                .rememberMe()//实现记住我功能
                .key("sxblog")//指定remember-me session加密key(即使重启服务器也可保持用户在线)
                .and()
                .formLogin()//表单登陆
                .loginPage("/login").permitAll() //用户未登陆，配置登陆页面接口或者json提示
                .loginProcessingUrl("/login").permitAll()//表单提交接口，默认也是login接口
                .usernameParameter("username")//指定登陆页面或前后端分离下发送请求的字段
                .passwordParameter("password")//指定登陆页面或前后端分离下发送请求的字段
                .successHandler(new AuthenticationSuccessHandler() { //定义认证成功后返回json信息
                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
                        Object principle = authentication.getPrincipal();
                        httpServletResponse.setContentType("application/json;charset=utf-8");
                        PrintWriter out = httpServletResponse.getWriter();
                        httpServletResponse.setStatus(200);
                        Map<String,Object> map = new HashMap<>();
                        map.put("status",200);
                        map.put("msg","登陆成功！");
                        ObjectMapper om = new ObjectMapper();
                        out.write(om.writeValueAsString(map));
                        out.flush();
                        out.close();
                    }
                })
                .failureHandler(new AuthenticationFailureHandler() { //登陆认证失败返回json响应信息
            @Override
            public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
                httpServletResponse.setContentType("application/json;charset=utf-8");
                PrintWriter out = httpServletResponse.getWriter();
                httpServletResponse.setStatus(401);
                Map<String,Object> map = new HashMap<>();
                map.put("status",401);
                if(e instanceof LockedException) {
                    map.put("msg","账户被锁定，登陆失败！");
                } else if (e instanceof BadCredentialsException) {
                    map.put("msg","用户名或密码错误，登陆失败！");
                }else if (e instanceof DisabledException) {
                    map.put("msg","账户被禁用，登陆失败！");
                }else if (e instanceof AccountExpiredException) {
                    map.put("msg","账户已过期，登陆失败！");
                }else if (e instanceof CredentialsExpiredException) {
                    map.put("msg","密码已过期，登陆失败！");
                } else {
                    e.printStackTrace();
                    map.put("msg","登陆失败！");
                }
                ObjectMapper om = new ObjectMapper();
                out.write(om.writeValueAsString(map));
                out.flush();
                out.close();
            }
        })
                .permitAll()//允许所有人访问登陆接口
                .and()
                .logout()//配置注销登陆
                .logoutUrl("/logout")//配置登出接口
                .clearAuthentication(true)//清除认证信息
                .invalidateHttpSession(true)//使Session失效
                .addLogoutHandler(new LogoutHandler() {
                    @Override
                    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
                    }
                })
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
                        httpServletResponse.setContentType("application/json;charset=utf-8");
                        PrintWriter out = httpServletResponse.getWriter();
                        httpServletResponse.setStatus(200);
                        Map<String,Object> map = new HashMap<>();
                        map.put("status",200);
                        map.put("msg","注销成功！");
                        ObjectMapper om = new ObjectMapper();
                        out.write(om.writeValueAsString(map));
                        out.flush();
                        out.close();
                    }
                })
                .and()
                .csrf()
                .disable()
                //关闭csrf安全保护
                .exceptionHandling() //未登陆状态返回json提示
                .authenticationEntryPoint((req, resp, authException) -> {
                            resp.setContentType("application/json;charset=utf-8");
                            PrintWriter out = resp.getWriter();
                            out.write("尚未登录，请先登录");
                            out.flush();
                            out.close();
                        }
                )
                .and()
                .sessionManagement()
                .maximumSessions(1)//只允许一台设备登陆(新的登录踢掉旧的登录)
                .maxSessionsPreventsLogin(true);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//            auth.inMemoryAuthentication()
//                    .withUser("root").password("$2a$10$8XXMNg8WQ8YlSIGGcgnaw./zrf2k6klkqXs0ezawj43VN7uh/m8Wu").roles("ADMIN","DBA")
//                    .and()
//                    .withUser("admin").password("$2a$10$8XXMNg8WQ8YlSIGGcgnaw./zrf2k6klkqXs0ezawj43VN7uh/m8Wu").roles("ADMIN","USER")
//                    .and()
//                    .withUser("alex").password("$2a$10$8XXMNg8WQ8YlSIGGcgnaw./zrf2k6klkqXs0ezawj43VN7uh/m8Wu").roles("USER");
        auth.userDetailsService(userService);
    }
}
