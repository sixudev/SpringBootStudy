package cn.blogsx.mapper;

import cn.blogsx.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IUserMapper {
    User findByUsername(String username);
    User findUserById(String Id);
}
