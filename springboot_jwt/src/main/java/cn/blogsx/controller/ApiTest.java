package cn.blogsx.controller;

import cn.blogsx.annotation.UserLoginToken;
import cn.blogsx.entity.User;
import cn.blogsx.service.TokenService;
import cn.blogsx.service.UserService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ApiTest {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;
    @Autowired
    TokenService tokenService;
    //登录
    @PostMapping("/login")
    public Object login(@RequestBody  User user){
        logger.info("login调用了-----");
        JSONObject jsonObject=new JSONObject();
        User userForBase=userService.findByUsername(user);
        if(userForBase==null){
            jsonObject.put("message","用户名或密码错误");
            return jsonObject;
        }else {
            if (!userForBase.getPassword().equals(user.getPassword())){
                jsonObject.put("message","用户名或密码错误");
                return jsonObject;
            }else {
                String token = tokenService.getToken(userForBase);
                jsonObject.put("code","100");
                jsonObject.put("token", token);
                jsonObject.put("user", userForBase);
                System.out.println("jwt加密成功！！！");
                return jsonObject;
            }
        }
    }


    @UserLoginToken
    @GetMapping("/getMessage")
    public Object getMessage(){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("code","100");
        jsonObject.put("msg","你已通过jwt验证");
        return  jsonObject;
    }
}
