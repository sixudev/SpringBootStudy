package cn.blogsx.controller;

import cn.blogsx.entity.Book;
import cn.blogsx.mapper.IBookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    private IBookMapper iBookMapper;

    @RequestMapping("/books")
    public List books() {
        List<Book> allBooks = iBookMapper.getAllBooks();
        return allBooks;
    }
}
