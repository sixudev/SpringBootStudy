package cn.blogsx.mapper;

import cn.blogsx.entity.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IBookMapper {
    int addBook(Book book);//增

    int deleteBookById(Integer id);//删
    Book getBooById(Integer id); //查
    int updateBooById(Book book);//改
    List<Book> getAllBooks();
}
