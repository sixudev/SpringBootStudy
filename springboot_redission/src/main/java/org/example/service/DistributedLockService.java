package org.example.service;

/**
 * 分布式锁服务接口
 */
public interface DistributedLockService {
    /**
     * 加锁
     * @param lockName
     * @return
     */
    Boolean lock(String lockName);

    /**
     * 解锁
     * @param lockName
     * @return
     */
    Boolean unlock(String lockName);
}
