package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RedissionApplication 分布式锁启动类
 */
@SpringBootApplication
public class RedissionApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedissionApplication.class,args);
    }
}
