package org.example;

import org.example.service.DistributedLockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试类
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedissionApplicationTest {
    private final String LOCK = "LOCK";

    @Autowired
    private DistributedLockService distributedLock;

    // 测试不释放锁
    @Test
    public void testLock() {
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                distributedLock.lock(LOCK);
            },"myThread"+i).start();
        }
    }


    // 实际业务开发使用分布式锁的方式
    public void post() {
        try {
            if (distributedLock.lock(LOCK)) {
                // 业务逻辑
                log.info("开始业务逻辑");
            } else {
                // 处理获取锁失败的逻辑
                log.info("获取锁失败");
            }
        } catch (Exception e) {
            log.error("处理异常：", e);
        } finally {
            distributedLock.unlock(LOCK);
        }
    }
}
