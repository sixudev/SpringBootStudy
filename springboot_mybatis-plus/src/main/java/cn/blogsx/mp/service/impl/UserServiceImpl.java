package cn.blogsx.mp.service.impl;

import cn.blogsx.mp.entity.User;
import cn.blogsx.mp.mapper.UserMapper;
import cn.blogsx.mp.service.UserService;
import cn.blogsx.mp.vo.CommonResultVo;
import cn.blogsx.mp.vo.UserVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author alex
 * @since 2020-11-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public UserVo userPage(Integer page, Integer limit) {
        IPage<User> sqlPage = new Page<>(page, limit);
        IPage<User> userIPage = userMapper.selectPage(sqlPage, null);
        List<User> records = userIPage.getRecords();
        UserVo userVo = new UserVo();
        userVo.setCurrent(page);
        userVo.setSize(limit);
        userVo.setTotal(userIPage.getTotal());
        userVo.setUserList(records);
        return userVo;
    }

    @Override
    public CommonResultVo<UserVo> userPageCommon(Integer page, Integer limit) {
        IPage<User> sqlPage = new Page<>(page, limit);
        IPage<User> userIPage = userMapper.selectPage(sqlPage, null);
        List<User> records = userIPage.getRecords();
        UserVo userVo = new UserVo();
        userVo.setCurrent(page);
        userVo.setSize(limit);
        userVo.setTotal(userIPage.getTotal());
        userVo.setUserList(records);
        CommonResultVo<UserVo> commonResultVo = new CommonResultVo<>();
        commonResultVo.setCode(200);//定义业务码
        commonResultVo.setMsg("查询成功");
        commonResultVo.setData(userVo);
        return commonResultVo;
    }
}
