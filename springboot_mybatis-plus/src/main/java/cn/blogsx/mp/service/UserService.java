package cn.blogsx.mp.service;

import cn.blogsx.mp.entity.User;
import cn.blogsx.mp.vo.CommonResultVo;
import cn.blogsx.mp.vo.UserVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author alex
 * @since 2020-11-13
 */
public interface UserService extends IService<User> {

    //分页查询
    UserVo userPage(Integer page, Integer limit);

    CommonResultVo<UserVo> userPageCommon(Integer page, Integer limit);
}
