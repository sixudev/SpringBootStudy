package cn.blogsx.mp.mapper;

import cn.blogsx.mp.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2020-11-13
 */
public interface UserMapper extends BaseMapper<User> {

}
