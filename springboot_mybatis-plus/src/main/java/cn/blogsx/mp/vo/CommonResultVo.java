package cn.blogsx.mp.vo;

import lombok.Data;

@Data
public class CommonResultVo<T> {
        private Integer code;
        private String msg;
        private T data;
}
