package cn.blogsx.mp.vo;

import cn.blogsx.mp.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class UserVo {
    private Integer current;
    private Integer size;
    private Long total;
    private List<User> userList;
}
