package cn.blogsx.mp.controller;


import cn.blogsx.mp.entity.User;
import cn.blogsx.mp.service.UserService;
import cn.blogsx.mp.vo.CommonResultVo;
import cn.blogsx.mp.vo.UserVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author alex
 * @since 2020-11-13
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/test/{id}")
    public String test(@PathVariable  Integer id) {
        User user = userService.getById(id);
        System.out.println(user);
        return "success";
    }

    //分页查询
    @RequestMapping("/all")
    public UserVo getAll(@RequestParam(required = false, defaultValue = "1") Integer page,
                         @RequestParam(required = false, defaultValue = "10") Integer limit) {
        return  userService.userPage(page,limit);
    }
    //通用返回结果
    @RequestMapping("/commonAll")
    public CommonResultVo<UserVo> getAllCommon(@RequestParam(required = false, defaultValue = "1") Integer page,
                                       @RequestParam(required = false, defaultValue = "10") Integer limit) {
        return  userService.userPageCommon(page,limit);
    }

}
