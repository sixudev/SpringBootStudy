package cn.ggq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 *
 * 1，编写监听器类，实现接口并重写方法，再方法中编写业务处理
 * 2.再这个类上添加@RocketMQMessageListener注解
 * 3.还需要添加@Component注解交给springBoot来管理
 *
 */
@RocketMQMessageListener(
    consumerGroup = "springboot-consumer", //指定消费组
    topic = "01-boot"
)
@Component
public class Consumer implements RocketMQListener<String> {
    /**
     * 监听指定的topic,如果有消息发送到topic中，消费消息 再onMessage
     * @param msg
     */
    @Override
    public void onMessage(String msg) {
        System.out.println("消息的内容为："+ msg);

    }
}
