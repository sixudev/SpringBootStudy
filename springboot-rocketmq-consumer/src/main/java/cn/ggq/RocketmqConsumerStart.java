package cn.ggq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketmqConsumerStart {
    public static void main(String[] args) {
        SpringApplication.run(RocketmqConsumerStart.class,args);
    }
}
