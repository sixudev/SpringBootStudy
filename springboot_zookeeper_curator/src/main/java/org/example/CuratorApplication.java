package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 */
@SpringBootApplication
public class CuratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(CuratorApplication.class,args);
    }
}
