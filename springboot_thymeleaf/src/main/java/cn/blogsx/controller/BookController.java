package cn.blogsx.controller;

import cn.blogsx.entity.Book;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    @RequestMapping("/books")
    public ModelAndView books() {
        List<Book> bookList = new ArrayList<>();
        Book book1 = new Book();
        book1.setId(1);
        book1.setAuthor("思绪");
        book1.setName("SpringBoot学习笔记");
        bookList.add(book1);

        Book book2 = new Book();
        book2.setId(2);
        book2.setName("SpringBoot学习笔记");
        book2.setAuthor("思绪");
        bookList.add(book2);

        ModelAndView mv = new ModelAndView();
        mv.addObject("books",bookList);
        return mv;
    }
}
