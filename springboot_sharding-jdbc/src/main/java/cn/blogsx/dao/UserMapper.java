package cn.blogsx.dao;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.blogsx.entity.User;

/**
 *
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
