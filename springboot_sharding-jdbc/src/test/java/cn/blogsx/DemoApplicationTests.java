package cn.blogsx;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.blogsx.dao.UserMapper;
import cn.blogsx.entity.User;

/**
 * 测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void contextLoads() {
        User user = new User();
        user.setId(1);
        user.setName("666");
        user.setAge(16);
        userMapper.insert(user);
    }

    @Test
    public void contextLoads2() {
        User user = new User();
        user.setId(2);
        user.setName("666");
        user.setAge(17);
        userMapper.insert(user);
    }

    @Test
    public void testQuery() {
        User user = userMapper.selectById(2);
        System.out.println(user);
    }
}
