package cn.blogsx.controller;

import cn.blogsx.dao.BookDao;
import cn.blogsx.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
public class BookController implements Serializable {
    @Autowired
    private BookDao bookDao;

    @RequestMapping("/book")
    public Book getBook() {
        return bookDao.getOne(1);
    }
}
