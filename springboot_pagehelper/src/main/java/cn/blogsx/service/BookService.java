package cn.blogsx.service;

import cn.blogsx.entity.Book;
import com.github.pagehelper.PageInfo;

public interface BookService {
    /**
     * PageHelper分页接口
     * @param pageNum
     * @param pageSize
     * @return PageInfo类
     */
    public PageInfo<Book> allBook(int pageNum, int pageSize);
}
