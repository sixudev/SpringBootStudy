package cn.blogsx.service.impl;

import cn.blogsx.entity.Book;
import cn.blogsx.mapper.IBookMapper;
import cn.blogsx.service.BookService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private IBookMapper iBookMapper;

    @Override
    public PageInfo<Book> allBook(int pageNum, int pageSize) {
        //开启分页
        PageHelper.startPage(pageNum,pageSize);  //pageNum为页码，pageSize为页面大小
        List<Book> bookList = iBookMapper.getAllBooks();
        PageInfo<Book> pageInfo = new PageInfo<Book>(bookList);
        return pageInfo;
    }
}
