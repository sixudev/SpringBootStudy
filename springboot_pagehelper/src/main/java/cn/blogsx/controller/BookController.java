package cn.blogsx.controller;

import cn.blogsx.entity.Book;
import cn.blogsx.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping("/books")
    public PageInfo<Book> books(Integer pageNum, Integer pageSize) {
        return bookService.allBook(pageNum,pageSize);
    }
}
