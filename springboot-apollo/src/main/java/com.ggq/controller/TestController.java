package com.ggq.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Value( "${name}" )
    private String name;

    @RequestMapping("/name")
    public String ReadName() {
        return "我的名字是："+this.name;
    }

}
