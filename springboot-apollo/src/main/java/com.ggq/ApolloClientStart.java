package com.ggq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;

@SpringBootApplication
@EnableApolloConfig //此处开启apollo配置
public class ApolloClientStart {
    public static void main(String[] args) {
        SpringApplication.run(ApolloClientStart.class,args);
    }
}
