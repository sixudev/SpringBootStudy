package org.example.service;

import org.example.entity.Order;

/**
 * 订单服务
 */
public interface OrderService {

    Order generateOrder(String userId);
}
