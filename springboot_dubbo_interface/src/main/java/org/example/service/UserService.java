package org.example.service;

import org.example.entity.User;

/**
 * 公共服务接口
 */
public interface UserService {

    User getUserById(String id);
}
