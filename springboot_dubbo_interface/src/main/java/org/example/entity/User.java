package org.example.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * 实体类
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 7481105315722515366L;

    private String id;

    private String name;

    private String address;

    @Override
    public String toString() {
        return "User{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", address='" + address + '\'' + '}';
    }
}
