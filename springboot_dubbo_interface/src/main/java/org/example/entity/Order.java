package org.example.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * 订单实体类
 */
@Data
public class Order implements Serializable {

    private static final long serialVersionUID = 3943978648604656103L;
    
    private String orderId;

    private String userId;

    private String userName;

    private String address;

    @Override
    public String toString() {
        return "Order{" + "orderId='" + orderId + '\'' + ", userId='" + userId + '\'' + ", userName='" + userName + '\''
            + ", address='" + address + '\'' + '}';
    }
}
