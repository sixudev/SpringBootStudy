package cn.ggq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketmqProducerStart {
    public static void main(String[] args) {
        SpringApplication.run(RocketmqProducerStart.class,args);
    }
}
