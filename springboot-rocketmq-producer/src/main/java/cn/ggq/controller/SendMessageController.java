package cn.ggq.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("/send")
    private String sendMessage(String msg) {
        // topic: tag
        rocketMQTemplate.syncSend("01-boot",msg);
        return "发送成功！";
    }

}
