package cn.blogsx.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2 //开启Swagger2
public class SwaggerConfig {
    @Bean
    Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.blogsx.controller"))//配置接口路径
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                .description("springboot整合swagger2测试文档")
                .contact(new Contact("思绪博客","https://www.blogsx.cn",
                        "304208719@qq.com"))
                .version("1.0")
                .title("api测试文档")
                .license("Apache2.0")
                .licenseUrl("http://www.baidu.com")
                .build());
    }
}
