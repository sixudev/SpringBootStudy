package cn.blogsx.controller;

import cn.blogsx.component.JmsComponent;
import cn.blogsx.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/active")
public class test {
    @Autowired
    JmsComponent jmsComponent;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        Message msg = new Message();
        msg.setContent("hello jms");
        msg.setDate(new Date());
        jmsComponent.send(msg);
        return "test seccess!";
    }
}
