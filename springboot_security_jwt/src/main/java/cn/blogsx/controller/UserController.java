package cn.blogsx.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping("/hello")
    public Object hello() {
        String str  = "hello";
        return str;
    }
    @PreAuthorize("hasRole('admin')")
    @RequestMapping("/admin/hello")
    public Object adminHello() {
        String str  = "/admin/hello";
        return str;
    }
}
