package cn.blogsx;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class test {
    public static void main(String[] args){
//        jiami();
        jiemi();
    }
    public static void jiami() {
        String token = Jwts.builder()
                //主题 放入用户名
                .setSubject("niceyoo")
                //自定义属性 放入用户拥有请求权限
                .claim("authorities","admin")
                //失效时间
                .setExpiration(new Date(System.currentTimeMillis() + 7 * 60 * 1000))
                //签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, "tmax")
                .compact();
        System.out.println(token);
    }

    public static void jiemi() {
        try {
            //解析token
            Claims claims = Jwts.parser()
                    .setSigningKey("tmax")
                    .parseClaimsJws("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuaWNleW9vIiwiYXV0aG9yaXRpZXMiOiJhZG1pbiIsImV4cCI6MTYwNzMzMjE1MH0.OwFYMBJZcBp8ApJEhZPdpAHa2qWBSXRzbyBZMKXKRWOL7HH8sExEn5WP32jW-XBCATYEX62Bcin6lsuiDmq5SQ")
                    .getBody();

            System.out.println(claims);
            //获取用户名
            String username = claims.getSubject();
            System.out.println("username:"+username);
            //获取权限
            String authority = claims.get("authorities").toString();
            System.out.println("权限："+authority);
        } catch (ExpiredJwtException e) {
            System.out.println("jwt异常");
        } catch (Exception e){
            System.out.println("异常");
        }
    }

}
