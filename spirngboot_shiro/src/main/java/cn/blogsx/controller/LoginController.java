package cn.blogsx.controller;

import cn.blogsx.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


@Slf4j
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }
    @PostMapping("/loginCheck")
    public String login(HttpSession session, String userName, String password,
                        @RequestParam(value = "rememberMe",required = false,defaultValue = "false") Boolean rememberMe) {
        User user = new User();
        user.setUserName(userName);
        user.setPassword(password);
        if (StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getPassword())) {
            return "请输入用户名和密码！";
        }
        //用户认证信息
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(
                user.getUserName(),
                user.getPassword(),
                rememberMe
        );
        try {
            //进行验证，这里可以捕获异常，然后返回对应信息
            subject.login(usernamePasswordToken);

        } catch (UnknownAccountException e) {
            log.error("用户名不存在！", e);
            return "redirect:/login";
        } catch (AuthenticationException e) {
            log.error("账号或密码错误！", e);
            return "redirect:/login";
        } catch (AuthorizationException e) {
            log.error("没有权限！", e);
            return "redirect:/login";
        }
        return "redirect:/index";
    }

    @RequiresRoles("admin")
    @GetMapping("/admin")
    @ResponseBody
    public String admin() {
        return "admin success!";
    }

    @RequestMapping({"/","/index"})
    public String index() {
        System.out.println("index方法执行了！");
        return "index";
    }

    @RequiresPermissions("add")
    @GetMapping("/add")
    @ResponseBody
    public String add() {
        return "add success!";
    }
    /**
     * 退出登录
     */
    @GetMapping("/logout")
    public String logout() {
        SecurityUtils.getSubject().logout();
        return "redirect:/login";
    }
}
