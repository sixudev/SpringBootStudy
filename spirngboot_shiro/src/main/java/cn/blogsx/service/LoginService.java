package cn.blogsx.service;

import cn.blogsx.entity.User;

public interface LoginService {
    User getUserByName(String userName);
}
