package org.example.lock;

/**
 * 分布式锁测试类
 */
public class DistributedLockTest {
    public static void main(String[] args) throws Exception {
        // 模拟5个客户端（即5个分布式线程）连接
        for (int i = 0; i < 5; i++) {
            DistributedLock lock1 = new DistributedLock();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        lock1.zkLock();
                        System.out.println("线程"+Thread.currentThread().getName()+"启动，获取到锁");
                        Thread.sleep(2 * 1000);

                        lock1.unZkLock();
                        System.out.println("线程"+Thread.currentThread().getName()+"释放锁");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            },"线程"+i).start();
        }
    }
}
