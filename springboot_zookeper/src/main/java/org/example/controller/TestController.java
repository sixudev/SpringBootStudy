package org.example.controller;

import java.util.List;

import org.example.service.ZkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author gaogq25917@yunrong.cn
 * @version V3.0
 * @since 2022/8/3 11:24
 */
@RestController
public class TestController {
    @Autowired
    private ZkService zkService;
    @RequestMapping("/allNode")
    public List<String> getAllNode() {
       return zkService.getChildren("/");
    }
}
