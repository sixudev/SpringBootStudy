SpringBoot学习-整合笔记

各demo模块作用

| **springboot_quick_start**   | **SringBoot快速入门**                                        |
| ---------------------------- | ------------------------------------------------------------ |
| **springboot_mybatis**       | **springboot整合mybatis**                                    |
| springboot_mybatisplus       | springboot整合mybatisplus                                    |
| springboot_pagehelper        | <a href="https://www.cnblogs.com/sxblog/p/13674997.html">springboot整合Mybatis之pagehelper插件</a> |
| springboot_generator         | springboot整合Mybatis之generator插件                         |
| springboot_jpa               | springboot整合Jpa                                            |
| springboot_thymeleaf         | springboot整合thymeleaf                                      |
| springboot_swagger2          | sprigboot整合swagger2                                        |
| springboot_redis             | springboot整合redis                                          |
| springboot_mongodb           | springboot整合mongodb                                        |
| spirngboot_shiro             | springboot整合shiro                                          |
| springboot_activemq          | springboot整合activemq                                       |
| springboot_kafka             | springboot整合kafka                                          |
| springboot_amqp              | springboot整合amqp                                           |
| springboot_email             | springboot整合email实现邮件发送                              |
| springboot_jetty             | springboot整合jetty                                          |
| springboot_undertow          | springboot整合undertow                                       |
| springboot_jwt               | springboot整合jwt（无安全框架入门jwt）                       |
| springboot_spring_security   | <a href="https://www.cnblogs.com/sxblog/p/13917674.html">[SpringBoot 整合Spring Security](https://www.cnblogs.com/sxblog/p/13917674.html)</a> |
| springboot_security_jwt      | <a href="https://www.cnblogs.com/sxblog/p/14108687.html">[SpringSecurity 整合JWT实现无状态登陆](https://www.cnblogs.com/sxblog/p/14108687.html)</a> |
| springboot_schedule          | springboot整合schedule实现简单定任务                         |
| springboot_uniform_api       | springboot统一json返回数据格式                               |
| springboot_websocket         | springboot整合websocket                                      |
| springboot-rocketmq-consumer | springboot整合rocketMQ                                       |
| springboot-rocketmq-producer | springboot整合rocketMQ                                       |

