package org.example;

import org.example.service.OssService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;

/**
 * 单元测试主类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OssServiceTest {
    @Autowired
    private OssService ossService;

    @Test
    public void testGetPresignedObjectUrl() throws Exception {
        String test = ossService.getPresignedObjectUrl("test", "1_1663921436748.png", 7);
        System.out.println(test);

    }
}
