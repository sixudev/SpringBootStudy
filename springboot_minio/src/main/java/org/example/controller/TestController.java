package org.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.example.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 */
@RestController
public class TestController {
    @Autowired
    private OssService ossService;
    @PostMapping("/upload")
    public Object upload(@RequestParam("file") MultipartFile file) {

        List<String> upload = ossService.upload(new MultipartFile[]{file});

        return upload.get(0);
    }

    @RequestMapping("/download/{fileName}")
    public ResponseEntity<byte[]> download(@PathVariable("fileName") String fileName) {
        ResponseEntity<byte[]> download = ossService.download(fileName);
        return download;
    }

    @RequestMapping("/getFileUrl/{fileName}")
    public String getFileUrl(@PathVariable("fileName") String fileName) {
        return ossService.getFileUrl(fileName);
    }
}
