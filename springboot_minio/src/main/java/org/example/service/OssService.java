package org.example.service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

/**
 * OSS服务接口
 */
public interface OssService {
    /**
     * 判断bucket是否存在，不存在则创建
     *
     * @param bucketName
     */
    void existBucket(String bucketName);

    /**
     * 创建桶
     *
     * @param bucketName
     * @return
     */
    Boolean makeBucket(String bucketName);

    /**
     * 删除桶
     *
     * @param bucketName
     * @return
     */
    Boolean removeBucket(String bucketName);

    /**
     * 上传文件
     *
     * @param multipartFile
     * @return
     */
    List<String> upload(MultipartFile[] multipartFile);

    /**
     * 下载文件
     *
     * @param fileName
     * @return
     */
    public ResponseEntity<byte[]> download(String fileName);

    public String getPresignedObjectUrl(String bucketName, String objectName, Integer expires) throws Exception;

    public String getFileUrl(String fileName);
}
