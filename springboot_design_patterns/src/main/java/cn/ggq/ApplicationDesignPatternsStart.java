package cn.ggq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 设计模式主启动类
 */
@SpringBootApplication
public class ApplicationDesignPatternsStart {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationDesignPatternsStart.class,args);
    }
}
