package cn.ggq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.ggq.service.strategy.impl.DiscountStrategyContentService;

/**
 * TODO
 *
 * @author gaogq25917@yunrong.cn
 * @version V3.0
 * @since 2023/2/12 17:06
 */
@RequestMapping("/discount")
@RestController
public class DiscountController {

    @Autowired
    private DiscountStrategyContentService discountStrategyContentService;

    @RequestMapping("/handler")
    private String handlerStrategy(String strategyType) {
        discountStrategyContentService.show(strategyType);
        return "ok";
    }
}
