package cn.ggq.service.strategy.inter;

/**
 * 打折抽象策略类
 */
public interface DiscountStrategy {
    public void show();
}
