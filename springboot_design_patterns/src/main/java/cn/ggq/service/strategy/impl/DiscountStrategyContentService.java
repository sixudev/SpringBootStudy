package cn.ggq.service.strategy.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.ggq.service.strategy.inter.DiscountStrategy;

/**
 * TODO
 *
 * @author gaogq25917@yunrong.cn
 * @version V3.0
 * @since 2023/2/12 16:50
 */
@Service
public class DiscountStrategyContentService {

    @Autowired
    private final Map<String, DiscountStrategy> strategyMap =  new ConcurrentHashMap<>();

    public DiscountStrategyContentService(Map<String, DiscountStrategy> strategyMap) {
        this.strategyMap.clear();
        strategyMap.forEach(strategyMap::put);
    }

    public void show(String strategyType) {
        if(strategyType== null) {
            return;
        }
        strategyMap.get(strategyType).show();
    }
}
