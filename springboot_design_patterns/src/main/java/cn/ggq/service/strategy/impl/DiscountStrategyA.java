package cn.ggq.service.strategy.impl;

import org.springframework.stereotype.Component;

import cn.ggq.service.strategy.inter.DiscountStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * TODO
 *
 * @author gaogq25917@yunrong.cn
 * @version V3.0
 * @since 2023/2/12 16:40
 */
@Slf4j
@Component("A")
public class DiscountStrategyA implements DiscountStrategy {

    @Override
    public void show() {
        log.info("买一送一");
    }
}
