package cn.blogsx.controller;

import cn.blogsx.component.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;

@RestController
public class SendEmail {
    @Autowired
    MailService mailService;

    @Autowired
    TemplateEngine templateEngine;

    /**
     * 访问该接口，自动发送邮件
     * @return
     */
    @RequestMapping("/send")
    public String sendEmail() {
        mailService.sendSimpleMail("168860786@qq.com","304208719@qq.com",
                null,"开发测试邮件","我是邮件内容");

        return "放送成功，请查收！";
    }
    /**
     * 访问该接口，自动发送邮件，并且附带附件
     * @return
     */

    @RequestMapping("/sendAttach")
    public String sendAttachFileMail() {
        mailService.sendAttachFileMail("168860786@qq.com","304208719@qq.com",
                "测试邮件主题","测试邮件内容",new File("D:\\test\\file\\邮件附件.docx"));
        return "发送带附件的邮件已成功,请查收！";
    }

    /**
     * 使用Thymeleaf模板进行构建邮件
     */

    @RequestMapping("/htmlMail")
    public String  sendHtmlMailThymeleaf() {
        Context context = new Context();
        context.setVariable("description","请点击邮件进行激活");
        String mail = templateEngine.process("mailtemplate.html", context);
        mailService.sendHtmlMail("168860786@qq.com","304208719@qq.com",
                "开发测试邮件",mail);
        return "发送成功，请注意查收！";
    }
}
